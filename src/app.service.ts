import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { lastValueFrom, map } from 'rxjs';
import * as cheerio from 'cheerio';
import { InjectModel } from '@nestjs/mongoose';
import { Product, ProductDocument } from './models/product.model';
import { Model } from 'mongoose';

@Injectable()
export class AppService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  @Cron(CronExpression.EVERY_10_SECONDS)
  async fetchDataFromUrl() {
    try {
      // fetch html from link
      const productPageHtml = await lastValueFrom(
        this.httpService
          .get('http://localhost:3000/products/page')
          .pipe(map((res) => res.data)),
      );

      // load to cheerio
      const $ = cheerio.load(productPageHtml);

      let allProducts = [];

      // extract data from html using cheerio
      $('div.productList')
        .find('ul.product')
        .each((_, productElement) => {
          allProducts.push({
            productId: $(productElement).find('ul > li.productId').text(),
            name: $(productElement).find('ul > li.productName').text(),
            description: $(productElement)
              .find('ul > li.productDescription')
              .text(),
            price: +$(productElement).find('ul > li.productPrice').text(),
          });
        });

      // insert the same
      await this.insertDataToDB(allProducts);
    } catch (error) {
      throw error;
    }
  }

  async insertDataToDB(allProducts: Product[]) {
    try {
      const totalCounts = await this.productModel.estimatedDocumentCount();

      // just a small filter to avoid continious inserts
      if (totalCounts !== allProducts.length && allProducts.length) {
        // ordered is set to false to ignore errors and insert data and catch to avoid logging
        await this.productModel
          .insertMany(allProducts, { ordered: false })
          .catch((_) => {});
      }
    } catch (error) {
      throw error;
    }
  }
}
